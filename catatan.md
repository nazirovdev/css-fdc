# Overide CSS

Terkadang kita ingin menerapkan styling pada sebuah elemen, yang tidak bergantun pada styling element parent-nya atau styling yang sudah diterapkan pada child itu sendiri, nah maka kita akan melakukan yang namannya Override CSS. Ada beberapa cara untuk melakukan overide pada css, diantaranya :

### Menggunakan ID

```
<style>
    body {
        color: red
    }

    #text-green {
        color: green
    }
</style>

<h1 id="text-green">Hello World<h1>
```

### Menggunakan Class

```
<style>
    body {
        color: red
    }

    .text-green {
        color: green
    }
</style>

<h1 class="text-green">Hello World<h1>
```

### Menggunakan class baru untuk menimpa class yang sudah ada

```
<style>
    body {
        color: red
    }

    .text-green {
        color: green
    }

    .text-red {
        color: red
    }
</style>

<h1 class="text-green text-red">Hello World<h1>
```

### Menggunakan !import sebagai penegasan bahwa, styling ini harus diterapkan

```
<style>
    body {
        color: red
    }

    .text-green {
        color: green !import
    }

    .text-red {
        color: red
    }
</style>

<h1 class="text-green text-red">Hello World<h1>
```
